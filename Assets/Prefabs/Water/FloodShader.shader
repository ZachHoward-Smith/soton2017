﻿Shader "Unlit/FloodShader"
{
	Properties
	{
		_MainTex ("Texture", 2D) = "white" {}
	}
	SubShader
	{
		Tags { "RenderType"="Opaque" }
		LOD 100

		Pass
		{
			Lighting On
			Tags{ "LightMode" = "ForwardBase" }
			CGPROGRAM
			#pragma vertex vert Standard
			#pragma fragment frag
			#pragma multi_compile_fwdbase
			#pragma multi_compile_fog
			// make fog work
			
			#include "UnityCG.cginc"
			#include "AutoLight.cginc"

			struct appdata
			{
				float4 vertex : POSITION;
				float2 uv : TEXCOORD0;
			};

			struct v2f
			{
				float2 uv : TEXCOORD0;
				float4 pos : SV_POSITION;
				UNITY_FOG_COORDS(1)
				SHADOW_COORDS(2)
			};

			sampler2D _MainTex;
			float4 _MainTex_ST;
			
			float GetWaveHeight(float2 uv) 
			{
				float2 tex = uv * 10.0 + float2(_Time.r, 0.0).xy;
				float value = tex2Dlod(_MainTex, float4(tex.x, tex.y, 0.0, 0.0)).r ;
				value += (1.0f + cos(uv * 100.0 + float2(_Time.r *60.0, 0.0).x))/ 10.0;
				return value;
			}

			v2f vert (appdata v)
			{
				v2f o;
				v.vertex.z += pow(GetWaveHeight(v.uv),5.0) * 4.0;
				o.pos = UnityObjectToClipPos(v.vertex);
				o.uv = TRANSFORM_TEX(v.uv, _MainTex);
				TRANSFER_SHADOW(o);
				UNITY_TRANSFER_FOG(o,o.pos); 
				return o;
			}
			
			fixed4 frag (v2f i) : SV_Target
			{
				// sample the texture
				float height = GetWaveHeight(i.uv);
				float4 foamColor = float4(1.0, 1.0, 1.0, 1.0);
				float4 waterColor = float4(0.0, 0.5, 0.6, 1.0);
				float atten = SHADOW_ATTENUATION(i);
				float divisions = 7.0;
				if (false) 
				{
					height *= divisions;
					height = round(height);
					height /= divisions;
				}
				float4 col = lerp(pow(height,4.0),foamColor, waterColor);
				UNITY_APPLY_FOG(i.fogCoord, col);
				return col * lerp(atten,1.0, 0.5);
			}
			ENDCG
		}
	}
	Fallback "VertexLit"
}
