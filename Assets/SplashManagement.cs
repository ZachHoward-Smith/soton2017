﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SplashManagement : MonoBehaviour
{
    public GameObject splashPrefab;
    private CameraScroller camera;

    bool isPlaying = false;
    bool doneCinematic = false;

	// Use this for initialization
	void Start ()
    {
        camera = GameObject.FindGameObjectWithTag("CamHolder").GetComponent<CameraScroller>();
	}
	
	// Update is called once per frame
	void Update ()
    {
        if (isPlaying)
        {
            if (transform.position.y < 1)
            {
                transform.position += Vector3.up * Time.deltaTime;
            }
            else
            {
                if (!doneCinematic)
                {
                    camera.cameraSpeedUpRate = 0.1f;
                    camera.cameraStartSpeed = 1;
                    doneCinematic = true;
                }
                transform.position = new Vector3(transform.position.x, camera.transform.position.y - 25, transform.position.z);
            }
        }
    }

    public void OnTriggerEnter(Collider other)
    {
        Instantiate(splashPrefab, other.transform.position, Quaternion.identity);

        if(other.tag == "Player")
        {
            PlayerController p = other.GetComponent<PlayerController>();
            p.InitiateKill();
        }
    }
    public void Initialize()
    {
        isPlaying = true;
    }
}
