﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HydrantManager : MonoBehaviour {

    public List<HydrantScript> hydrants = new List<HydrantScript>();
    public float sprayInterval = 0;
    public bool reverse = false;

    bool shouldRun = false;
    float introStartTime;
    float intervalStartTime;
    int index = 0;

	// Use this for initialization
	void Start ()
    {
	}
	
	// Update is called once per frame
	void Update ()
    {
		if(shouldRun)
        {
            if (Time.fixedTime - intervalStartTime > sprayInterval)
            {
                if (index < hydrants.Count)
                {
                    hydrants[reverse ? (hydrants.Count - 1) - index : index].Initialize();
                    index++;
                    intervalStartTime = Time.fixedTime;
                }
                else
                {
                    index = 0;
                }
            }
            if(Time.fixedTime - introStartTime > 25)
            {
                for(int i = 0; i < hydrants.Count; i++)
                {
                    hydrants[i].Terminate();
                }
                shouldRun = false;
            }
        }
	}

    public void Initialize()
    {
        introStartTime = Time.fixedTime;
        intervalStartTime = Time.fixedTime;
        shouldRun = true;
    }
}
