﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class GameManager : MonoBehaviour
{
    public static GameManager GM;

    
    public List<PlayerController> players = new List<PlayerController>();
    public GameObject playerPrefab;

    public Material[] playerMaterials;

    public AudioClip music;
    // Use this for initialization

    void Awake()
    {
        GM = this;
    }

	void Start ()
    {
        GM = this;
    }
	
	// Update is called once per frame
	void Update ()
    {
		
	}

    IEnumerator resertGame()
    {
        float timer = 5;
        while(timer >= 0)
        {
            timer -= Time.deltaTime;
            yield return null;
        }
        SceneManager.LoadScene(0, LoadSceneMode.Single);
    }

    public void TestForGameOver()
    {
        //print("Test for Game Over");
        int numPlayersAlive = 0;
        int livingPlayer = 0;

        for(int i = 0; i < players.Count; i++)
        {
            if(players[i].lives > 0)
            {
                ++numPlayersAlive;
                livingPlayer = i;
                if(numPlayersAlive > 1)
                {
                    break;
                }
            }
        }

        if(numPlayersAlive == 1)
        {
            // thatplayer has won
            players[livingPlayer].allowInput = false;
            ObsticleController.OC.gameisOver = true;
            players[livingPlayer].transform.position = new Vector3(0, ObsticleController.OC.transform.position.y, 0);
            players[livingPlayer].winner = true;
            players[livingPlayer].gameObject.AddComponent<PlayerVictory>();
            players[livingPlayer].GetComponent<Rigidbody>().velocity = Vector3.zero;
            CameraScroller cs = FindObjectOfType<CameraScroller>();
            players[livingPlayer].enabled = false;
            cs.cameraSpeedUpRate = 0;
            cs.cameraSpeed = 0;
            StartCoroutine(resertGame());
        }

    }

    public void InitializePlayers(int[] ids)
    {
        for(int i = 0; i < Input.GetJoystickNames().Length; i++)
        {
            GameObject g = Instantiate(playerPrefab, new Vector3(-20 + (i* 10), 1, 0), Quaternion.identity);
            players.Add(g.GetComponent<PlayerController>());
            players[i].Initialize(i);
            foreach(GameObject gObject in players[i].materialObject)
            {
                gObject.GetComponent<Renderer>().material = playerMaterials[ids[i]];
            }
        }
    }
}
