﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerProjectile : MonoBehaviour
{

    GameManager gm;

    const float SPEED = 100;
    private int parentID = -1;
    private Rigidbody missile;

	// Use this for initialization
	void Start ()
    {
        gm = GameManager.GM;
        missile = GetComponent<Rigidbody>();
	}
	
	// Update is called once per frame
	void Update ()
    {
        if (parentID >= 0)
        {
            float angle = -1;

            Vector3 travelDirection = transform.forward;
            Vector3 offsetDirection = Vector3.zero;
            for (int i = 0; i < gm.players.Count; i++)
            {
                if (i != parentID && gm.players[i].lives > 0)
                {
                    Vector3 tempDirection = gm.players[i].transform.position - transform.position;
                    float curAngle = Vector3.Angle(travelDirection, tempDirection);

                    if (curAngle < 20.0f)
                    {
                        if (angle != -1)
                        {
                            // Prioritise the player most level with the shooter.
                            angle = (curAngle < angle ? curAngle : angle);
                        }
                        else
                        {
                            offsetDirection = tempDirection;
                            angle = curAngle;
                        }
                    }
                }
            }
            if (angle >= 0 && angle < 20)
            {
                missile.AddForce(offsetDirection.normalized * Time.deltaTime * SPEED * 10, ForceMode.Force);
            }
            else
            {
                missile.AddForce(transform.forward * Time.deltaTime * SPEED * 10, ForceMode.Force);
            }
        }

	}

    public void Initialize(int parentID)
    {
        this.parentID = parentID;
    }

    public void OnCollisionEnter(Collision collider)
    {
        if(collider.gameObject.tag == "Player")
        {
            PlayerController p = collider.gameObject.GetComponent<PlayerController>();
            if(p.ID != parentID)
            {
                p.ApplyDamage(0.25f);
                // Instantiate explosion particle
            }
        }
        Destroy(gameObject);
    }
}
