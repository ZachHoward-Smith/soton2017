﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerController : MonoBehaviour
{
    // Public Variables
    public int bufferCount;

    public float heightAid;
    public float maxClimbSpeed;
    public float maxFallSpeed;
    public float maxHorizontalSpeed;
    public Renderer[] meshRenderers;
    public Color[] meshColors;

    public GameObject[] materialObject;

    public GameObject missilePrefab;

    // Private Constants
    const float VSPEED = 50000;
    const float HSPEED = 500;
    const float BENCHMARK = 2;
    const float PI2 = Mathf.PI * 2.0f;
    const float PIDIV2 = Mathf.PI / 2.0f;
    const float SAFETIME = 0.0f;
    const float TURNSPEED = 10.0f;
    const float KILLDELAY1 = 3.0f;
    const float KILLDELAY2 = 2.0f;

    // Private Variables
    private Rigidbody player;
    float lastAngle;
    float angularSpeed;
    int playerID;
    string inputString = "";
    string controller = "";
    private float healthMultiplier = 1.0f;
    private List<GameObject> missiles = new List<GameObject>();

    private float fireDelayStart;
    private float fireDelayDuration = 1.0f;

    List<float> pastValues = new List<float>();
    float totalValue = 0;

    float stopTimer = -1;
    bool canGoSafe = true;

    public bool allowInput = true;

    public int lives = 3;
    public bool winner = false;

    private KeepOnScreen keepOnScreen;

    private bool hasLostLife = false;

    // Use this for initialization
    void Start()
    {
        stopTimer = Time.fixedTime;
        print(Input.GetJoystickNames().Length + " controllers detected.");

        for (int i = 0; i < bufferCount; i++)
        {
            pastValues.Add(0);
        }
        player = GetComponent<Rigidbody>();

        keepOnScreen = GetComponent<KeepOnScreen>();
        meshRenderers = GetComponentsInChildren<Renderer>();
    }

    public void Initialize(int playerID)
    {
        this.playerID = playerID;

        controller = Input.GetJoystickNames()[playerID].ToLower().Contains("xbox") ? "XB" : "PS";
        inputString = "P" + (playerID + 1) + "_" + controller + "_";
        MenuController.mc.UpdateLifeCount(playerID, lives);

    }

    // Update is called once per frame
    void FixedUpdate()
    {
        if (allowInput)
        {
            HandleInput();
            HandleSwivel();
            HandleHorizontalMovement();
            HandleVerticalMovement();
            ClampVelocity();
        }
        else
        {
            if (!winner)
            {
            HandleRespawn();
        }
        }
        RotateHelicopter();
        HandleRepair();

        if (Time.fixedTime - fireDelayStart > fireDelayDuration)
        {
            fireDelayStart = -1;
        }
    }
    void HandleInput()
    {
        if (Input.GetAxis(inputString + "A") == 1.0f && fireDelayStart < 0)
        {
            MyPrint("Firing Missile", "");

            // Bug here if a missile is fired during a rotation.
            Vector3 spawnPos = transform.position + new Vector3(0.307481f, -1.771333f, 0);
            GameObject g = Instantiate(missilePrefab, spawnPos, Quaternion.Euler(0, 90, 0), ObsticleController.OC.transform);
            g.transform.forward = transform.right;
            Physics.IgnoreCollision(GetComponent<Collider>(), g.GetComponent<Collider>(), true);
            PlayerProjectile p = g.GetComponent<PlayerProjectile>();
            p.Initialize(playerID);
            fireDelayStart = Time.fixedTime;
        }
    }

    void MyPrint(string button, object value)
    {
        //print("[Player" + i + " - " + controller + "] " + button + ": " + value) ;
    }

    float currentAverage = 0;
    void HandleSwivel()
    {

        if (player.velocity.y > 0)
            canGoSafe = true;
        
        string controller = Input.GetJoystickNames()[playerID].ToLower().Contains("xbox") ? "XB" : "PS";
        float valH = Input.GetAxis(inputString + "RHorizontal");
        float valV = Input.GetAxis(inputString + "RVertical");

        float mag = Mathf.Sqrt(Mathf.Pow(valH, 2) + Mathf.Pow(valV, 2));

        // Check to ensure the stick is fully extended at an angle
        if (mag > 0.85f)
        {
            float angle = Mathf.Atan2(valV, valH) + Mathf.PI;

            if (lastAngle == -1)
            {
                lastAngle = angle;
            }
            else
            {
                if (lastAngle > angle + Mathf.PI)
                {
                    lastAngle -= PI2;
                }

                if (angle > lastAngle + Mathf.PI)
                {
                    lastAngle += PI2;
                }

                float offset = (angle - lastAngle);

                PushValue(offset);

                if (offset > 0)
                {
                    angularSpeed = currentAverage * Time.deltaTime;
                    //print(angularSpeed);
                }
                else
                {
                    angularSpeed = 0;
                }

                lastAngle = angle;
            }

        }
        else
        {
            lastAngle = -1;
            angularSpeed = 0;
        }



    }

    void HandleVerticalMovement()
    {
        // If we're spinning fast enough
        if (angularSpeed > BENCHMARK / 1000)
        {
            stopTimer = -1;
            Vector3 acceleration = Vector3.up * angularSpeed * angularSpeed * VSPEED * healthMultiplier * Time.fixedDeltaTime;

            // Aid the player if they're falling
            player.AddForce(acceleration * (player.velocity.y < 0 ? heightAid : 1),ForceMode.VelocityChange);

        }
        else // Otherwise
        {
            player.AddForce(Vector3.down * 2.0f * Time.fixedDeltaTime, ForceMode.VelocityChange);
            if (stopTimer == -1)
            {
                // Start the safety timer if it's not already been started
                stopTimer = Time.fixedTime;
            }
            else
            {
                // If the safety timer is valid
                if (Time.fixedTime - stopTimer < SAFETIME)
                {
                    // canGoSafe prevents instantly resetting the speed when falling
                    if (player.velocity.y < 0 && canGoSafe)
                    {
                        Vector3 velocity = player.velocity;
                        velocity.y = 0;
                        player.velocity = velocity;
                    }
                }
                else
                {
                    player.AddForce(Vector3.down * 0.1f * Time.fixedDeltaTime, ForceMode.VelocityChange);
                    canGoSafe = false;
                }
            }
        }
    }

    void HandleHorizontalMovement()
    {
        string controller = Input.GetJoystickNames()[playerID].ToLower().Contains("xbox") ? "XB" : "PS";
        float valH = Input.GetAxis(inputString + "LHorizontal");

        if(valH != 0)
        {
            player.AddForce(valH * HSPEED * Time.fixedDeltaTime, 0, 0);
        }
        else
        {
            if (Mathf.Abs(player.velocity.x) > 0)
            {
                player.AddForce((player.velocity.x > 0 ? -1 : 1) * 10.0f, 0, 0);

                Vector3 storage = player.velocity;
                storage.x *= 0.95f;
                if(storage.x < 0)
                {
                    storage.x = storage.x > -0.2f ? 0 : storage.x;
                }
                else
                {
                    storage.x = storage.x < 0.2f ? 0 : storage.x;
                }
                
                player.velocity = storage;
                
            }
        }
    }


    public void PushValue(float value)
    {
        totalValue -= pastValues[0];
        totalValue += value;

        pastValues.RemoveAt(0);
        pastValues.Add(value);

        currentAverage = totalValue / bufferCount;
    }
    public void ClampVelocity()
    {
        Vector3 storage = player.velocity;
        if (player.velocity.y > maxClimbSpeed)
        {
            storage.y = maxClimbSpeed;
            player.velocity = storage;
        }
        else if (player.velocity.y < -(maxFallSpeed))
        {
            storage.y = -maxFallSpeed;
            player.velocity = storage;
        }

        if (player.velocity.x > maxHorizontalSpeed)
        {
            storage.x = maxHorizontalSpeed;
            player.velocity = storage;
        }
        else if (player.velocity.x < -(maxHorizontalSpeed))
        {
            storage.x = -maxHorizontalSpeed;
            player.velocity = storage;
        }
    }

    Quaternion destQuat;
    bool rotating;

    public void RotateHelicopter()
    {
        bool moveRight = player.velocity.x > 0;
        float percentage = player.velocity.x / maxHorizontalSpeed;
        if (!rotating)
        {
            if (Mathf.Abs(player.velocity.x) >= 0.25f)
            {
                //transform.rotation = Quaternion.Euler(0, moveRight ? 0 : 180, 20 * percentage * (moveRight ? -1 : 1));
                Vector3 rotVector = new Vector3(0, moveRight ? 0 : 180, 20 * percentage * (moveRight ? -1 : 1));
                destQuat = Quaternion.Euler(rotVector);
                rotating = true;
            }
        }
        else
        {
            transform.rotation = Quaternion.Lerp(transform.rotation, destQuat, Time.fixedDeltaTime * TURNSPEED);

            if(Quaternion.Angle(destQuat, transform.rotation) < 1.0f)
            {
                rotating = false;
            }
        }

    }

    public float GetRotorSpeed()
    {
        return angularSpeed * 100.0f;

    }
    

    public void ApplyDamage(float value)
    {
        player.velocity = Vector3.zero;
        healthMultiplier -= value;
        healthMultiplier = Mathf.Clamp01(healthMultiplier);
    }

    public int ID
    {
        get
        {
            return playerID;
        }
    }
    bool wasDamaged = false;
    float healTime = 0;
    private void HandleRepair()
    {
        if(wasDamaged)
        {
            healthMultiplier += Time.fixedDeltaTime / 20.0f;
        }

        if(healthMultiplier < 1 && !wasDamaged)
        {
            healTime = Time.fixedTime;
            wasDamaged = true;
        }

        if (healthMultiplier >= 1 && wasDamaged == true) 
        {
           // print(Time.fixedTime - healTime);
        }
    }

    private float killDelayStartTime;
    private float respawnStep = 0;

    public void InitiateKill()
    {
        if (!hasLostLife)
        {
            //collider.enabled = false;
            gameObject.layer = 20 + playerID;
            allowInput = false;
            killDelayStartTime = Time.fixedTime;
            keepOnScreen.enabled = false;
            --lives;
            hasLostLife = true;
            MenuController.mc.UpdateLifeCount(playerID, lives);
            if (lives <= 0)
            {
                GameManager.GM.TestForGameOver();
            }
        }
    }
    int alphaValue = 0;

    private void HandleRespawn()
    {
        if (lives > 0)
        {
            if (respawnStep == 0)
            {
                if (Time.fixedTime - killDelayStartTime > KILLDELAY1)
                {
                    Camera c = GameObject.FindGameObjectWithTag("MainCamera").GetComponent<Camera>();
                    Vector3 spawnPos = c.ScreenToWorldPoint(new Vector3(0.5f, 1.0f, 0));
                    spawnPos.y += 15;
                    spawnPos.z = 0;
                    transform.position = spawnPos;
                    respawnStep = 1;
                    killDelayStartTime = Time.fixedTime;
                    player.velocity = new Vector3(0, c.GetComponentInParent<CameraScroller>().GetSpeed() - 10.0f, 0);
                    meshColors = new Color[meshRenderers.Length];
                    for (int i = 0; i < meshRenderers.Length; i++)
                    {
                        if (!meshRenderers[i].material.name.Contains("Blur"))
                        {
                            meshColors[i] = meshRenderers[i].material.color;
                        }
                    }
                }
            }
            else
            {
                if (Time.fixedTime - killDelayStartTime > KILLDELAY2)
                {
                    gameObject.layer = 0;
                    hasLostLife = false;
                    allowInput = true;
                    healthMultiplier = 1.0f;
                    respawnStep = 0;
                    keepOnScreen.enabled = true;
                    for (int i = 0; i < meshRenderers.Length; i++)
                    {
                        if (!meshRenderers[i].material.name.Contains("Blur"))
                        {
                            meshRenderers[i].material.color = meshColors[i];
                        }
                    }
                }
                else
                {
                    player.AddForce(0, 4.0f * Time.fixedDeltaTime, 0, ForceMode.VelocityChange);

                    float perc = (Time.fixedTime - killDelayStartTime) / KILLDELAY2;
                    float myVal = Mathf.Abs(Mathf.Sin(Mathf.PI * 3 * perc));

                    for (int i = 0; i < meshRenderers.Length; i++)
                    {
                        if (!meshRenderers[i].material.name.Contains("Blur"))
                        {
                            meshRenderers[i].material.color = meshColors[i] + (Color.white * myVal);
                        }
                    }
                }
            }
        }
    }

    public void OnCollisionEnter(Collision other)
    {
        float value = 0;
        
        switch(other.gameObject.tag)
        {
            case "Player":
                value = other.impulse.magnitude;
                break;
            case "Missile":
                value = 10;
                break;
        }
        player.AddExplosionForce(value, other.contacts[0].point, 0, 0, ForceMode.VelocityChange);
    }
}
