﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HelicopterAnimator : MonoBehaviour {

    public GameObject rotorTop;
    public GameObject rotorSide;

    public Renderer rotorTopDisk;
    public Renderer rotorSideDisk;

    private float rotorMaxSpeed = 1000.0f;
    private float speed = 0;
    private PlayerController controller;

    // Use this for initialization
    void Start () {
        controller = GetComponent<PlayerController>();
        if (controller == null)
        {
            Debug.Log("HelicopterAnimator - player controller is not valid");
        }
	}
	
	// Update is called once per frame
	void Update () {
       
        float rotSpeed = 0;
        if (controller != null)
        {
            float controllerSpeed = controller.GetRotorSpeed();
            GetComponent<AudioSource>().volume = Mathf.Clamp01(controllerSpeed);
            if (speed > controllerSpeed)
            {
                speed = Mathf.Lerp(speed, controllerSpeed, Time.deltaTime * 1.0f);
            } else
            {
                speed = Mathf.Lerp(speed, controllerSpeed, Time.deltaTime * 10.0f);
            }
            rotSpeed = speed * rotorMaxSpeed *Time.deltaTime;
        }
        if (rotorTop != null)
        {
            rotorTop.transform.Rotate(new Vector3(0.0f, rotSpeed, 0.0f));
            rotorTopDisk.material.SetFloat("_Blend", speed);
        }
        if (rotorSide != null)
        {
            rotorSide.transform.Rotate(new Vector3(rotSpeed, 0.0f, 0.0f));
            rotorSideDisk.material.SetFloat("_Blend", speed);
        }
    }
}
