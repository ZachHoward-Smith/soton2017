﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BuildingCreator : MonoBehaviour
{

    public enum SelectionType { Random, InOrder }

    public List<GameObject> bottomTiles;

    public List<GameObject> middleTiles;
    public SelectionType middleTileSelection;
    public bool middleTileCountFromSelection = false;
    public int minimumMiddleTiles = 1;
    public int maximumMiddleTiles = 1;

    public List<GameObject> roofTiles;

    public Vector3 floorOffset = new Vector3(0.0f, 3.0f, 0.0f);

    void Awake()
    {
        Vector3 floorPosition = new Vector3();
        if (bottomTiles.Count > 0)
        {
            AddFloor(ref floorPosition, bottomTiles[Random.Range(0, bottomTiles.Count)]);
        }
        if (middleTiles.Count > 0)
        {
            int middleTilesCount = 0;
            if (middleTileCountFromSelection)
            {
                middleTilesCount = middleTiles.Count;
            }
            else
            {
                middleTilesCount = Random.Range(minimumMiddleTiles, maximumMiddleTiles + 1);
            }
            int floorIndex = 0;
            switch (middleTileSelection)
            {
                case SelectionType.Random:
                    floorIndex = Random.Range(0, middleTiles.Count);
                    break;
                case SelectionType.InOrder:
                default:
                    floorIndex = 0;
                    break;
            }
            for (int i = 0; i < middleTilesCount; i++)
            {
                AddFloor(ref floorPosition, middleTiles[floorIndex]);
                switch (middleTileSelection)
                {
                    case SelectionType.Random:
                        floorIndex = Random.Range(0, middleTiles.Count);
                        break;
                    case SelectionType.InOrder:
                    default:
                        floorIndex++;
                        floorIndex = floorIndex % middleTiles.Count;
                        break;
                }
            }
        }
        if (roofTiles.Count > 0)
        {
            AddFloor(ref floorPosition, roofTiles[Random.Range(0, roofTiles.Count)]);
        }
    }

    void AddFloor(ref Vector3 position, GameObject floorPrefab)
    {
        GameObject floor = GameObject.Instantiate(floorPrefab, position, Quaternion.identity);
        floor.transform.SetParent(transform, false);
        position += floorOffset;
    }

    // Use this for initialization
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {

    }
}
