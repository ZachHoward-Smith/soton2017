﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;
using SM;
using Condition;

public class MenuController : MonoBehaviour
{
    public static MenuController mc;

    StateMachine MenuMachine;

    public EventSystem eventSystem;
    public Button StartGameButton;

    public GameObject MainMenuUI;
    public GameObject PlayerselectScreenUI;
    public GameObject PlayingGameUI;

    public GameObject[] HeliSelects;
    public GameObject[] ControllerSelects;
    public GameObject[] ControllerAreas;

    public GameObject[] PlayingGameHelis;

    //public GameObject[] ContLocs;
    public GameObject[] DefaultLocs;
    public GameObject[] Locks;

    public Text[] LivesTexts;

    List<float> InputDelayTimers = new List<float>();

    List<int> PlayerLocations = new List<int>();
    List<bool> PlayersLocked = new List<bool>();

    bool StartGame = false;
    bool ReturnToMenu = false;

    void Start()
    {
        mc = this;
        SetupStateMachine();
    }

    void Update()
    {
        MenuMachine.SMUpdate();
    }

    void BeginMainMenu()
    {
        MainMenuUI.SetActive(true);
        eventSystem.SetSelectedGameObject(StartGameButton.gameObject);
    }

    public void StartGameButtonFunc()
    {
        StartGame = true;
    }

    public void QuitGame()
    {
        Application.Quit();
    }

    void EndMainMenu()
    {
        MainMenuUI.SetActive(false);
    }

    void BeginPlayerSelection()
    {
        PlayerselectScreenUI.SetActive(true);
        for(int i = 0; i < HeliSelects.Length; ++i)
        {
            HeliSelects[i].SetActive(false);
            ControllerSelects[i].SetActive(false);
            ControllerAreas[i].SetActive(false);
        }

        PlayerLocations.Clear();
        PlayersLocked.Clear();
        InputDelayTimers.Clear();

        for (int i = 0; i < (Input.GetJoystickNames().Length <=4  ? Input.GetJoystickNames().Length : 4); ++i)
        {
            ControllerSelects[i].SetActive(true);
            ControllerSelects[i].transform.position = DefaultLocs[i].transform.position;
            ControllerAreas[i].SetActive(true);
            HeliSelects[i].SetActive(true);
            PlayerLocations.Add(-1);
            PlayersLocked.Add(false);
            InputDelayTimers.Add(0);
        }
    }

    void PlayerselectionUpdate()
    {
        for(int i = 0; i < InputDelayTimers.Count; ++i)
        {
            InputDelayTimers[i] -= Time.deltaTime;
        }

        //Input.GetAxis("P[contID]_[ContType(XB/PS)]_RHorizontal");
        string[] JoyNames = Input.GetJoystickNames();
        for(int i = 0; i < JoyNames.Length; ++i)
        {
            if (InputDelayTimers[i] <= 0)
            {
                if (!PlayersLocked[i])
                {
                    float axisVal = Input.GetAxis("P" + (i + 1) + "_" + (JoyNames[i].Contains("XBOX") ? "XB" : "PS") + "_RHorizontal");
                    if (Mathf.Abs(axisVal) > 0.5f)
                    {
                        do
                        {
                            ++PlayerLocations[i];
                            if (PlayerLocations[i] >= JoyNames.Length)
                            {
                                PlayerLocations[i] = -1;
                            }
                        } while (IsIDTaken(i));
                        PlaceController(i);
                        InputDelayTimers[i] = 0.5f;
                    }
                }
            }

            if(Input.GetAxis("P" + (i + 1) + "_" + (JoyNames[i].Contains("XBOX") ? "XB" : "PS") + "_A") > 0.5)
            {
                if (PlayerLocations[i] != -1)
                {
                    PlayersLocked[i] = true;
                    Locks[i].SetActive(true);
                }
            }

            if (Input.GetAxis("P" + (i + 1) + "_" + (JoyNames[i].Contains("XBOX") ? "XB" : "PS") + "_B") > 0.5)
            {
                if (PlayerLocations[i] != -1)
                {
                    PlayersLocked[i] = false;
                    Locks[i].SetActive(false);
                }                
            }

            if (Input.GetAxis("P" + (i + 1) + "_" + (JoyNames[i].Contains("XBOX") ? "XB" : "PS") + "_Y") > 0.5)
            {
                ReturnToMenu = true;
            }

        }
    }

    void PlaceController(int PlayerID)
    {
        if(PlayerLocations[PlayerID] == -1)
        {
            ControllerSelects[PlayerID].transform.position = DefaultLocs[PlayerID].transform.position;
        }
        else
        {
            ControllerSelects[PlayerID].transform.position = ControllerAreas[PlayerLocations[PlayerID]].transform.position;
        }
    }

    void EndPlayerSelection()
    {
        PlayerselectScreenUI.SetActive(false);
    }

    void BeginPlayingGame()
    {
        PlayingGameUI.SetActive(true);

        for(int i = 0; i < HeliSelects.Length; ++i)
        {
            PlayingGameHelis[i].SetActive(false);
        }

        for (int i = 0; i < (Input.GetJoystickNames().Length <= 4 ? Input.GetJoystickNames().Length : 4); ++i)
        {
            PlayingGameHelis[i].SetActive(true);
        }

        // spawn players
        GameManager.GM.InitializePlayers(PlayerLocations.ToArray());

        ObsticleController.OC.StartGame();
        //BeginGame();
        FindObjectOfType<CameraScroller>().BeginGame();

        GameManager.GM.GetComponent<AudioSource>().PlayOneShot(GameManager.GM.music, 0.7F);
    }

    void PlayingGameUpdate()
    {

    }

    void EndPlayingGame()
    {
        PlayingGameUI.SetActive(false);
    }

    bool IsIDTaken(int id)
    {
        if(PlayerLocations[id] == -1)
        {
            return false;
        }

        bool taken = false;

        for (int i = 0; i < PlayerLocations.Count; ++i)
        {
            if (i != id)
            {
                if (PlayerLocations[i] == PlayerLocations[id])
                {
                    taken = true;
                }
            }        
        }
        return taken;
    }

    public void UpdateLifeCount(int PlayerID, int Lives)
    {
        LivesTexts[PlayerLocations[PlayerID]].text = Lives.ToString();
    }

    #region State machine Conditions

    bool LeaveMenu()
    {
        return StartGame;
    }

    bool ArePlayersChosen()
    {
        //test if each player is locked in.
        bool AllplayersLocked = true;

        for(int i = 0; i < PlayersLocked.Count; ++i)
        {
            if (!PlayersLocked[i])
            {
                AllplayersLocked = false;
            }
        }

        return AllplayersLocked;
    }

    bool ShouldWeRetrunToMenu()
    {
        return ReturnToMenu;
    }

    void LeaveMenutransFunc()
    {
        StartGame = false;
    }

    void ReturnToMenuTransFunc()
    {
        ReturnToMenu = false;
    }

    #endregion

    void SetupStateMachine()
    {
        // Setup conditions
        BoolCondition LeaveMenuCond = new BoolCondition(LeaveMenu);
        BoolCondition PlayersChosenCond = new BoolCondition(ArePlayersChosen);
        BoolCondition ReturnToMenuCond = new BoolCondition(ShouldWeRetrunToMenu);

        // setup Transitions
        Transition LeaveMenuTrans = new Transition("Leave Menu", LeaveMenuCond, LeaveMenutransFunc);
        Transition GameStart = new Transition("Start Game", PlayersChosenCond);
        Transition ReturnToMenu = new Transition("Return To Menu", ReturnToMenuCond, ReturnToMenuTransFunc);

        // Setup States
        State MainMenu = new State("Main Menu",
            new List<Transition>() { LeaveMenuTrans },
            new List<Action>() { BeginMainMenu },
            new List<Action>(),
            new List<Action>() { EndMainMenu });

        State PlayerSelection = new State("Player Selection",
            new List<Transition>() { GameStart, ReturnToMenu },
            new List<Action>() { BeginPlayerSelection },
            new List<Action>() { PlayerselectionUpdate },
            new List<Action>() { EndPlayerSelection });

        State PlayingGame = new State("Playing Game",
            null,
            new List<Action>() { BeginPlayingGame },
            new List<Action>(),
            new List<Action>() { EndPlayingGame });

        // Add Target States
        LeaveMenuTrans.SetTargetState(PlayerSelection);
        GameStart.SetTargetState(PlayingGame);
        ReturnToMenu.SetTargetState(MainMenu);

        // Setup Machine
        MenuMachine = new StateMachine(null, MainMenu, PlayerSelection, PlayingGame);
        MenuMachine.InitMachine();
    }
        
}
