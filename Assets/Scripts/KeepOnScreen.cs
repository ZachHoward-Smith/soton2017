﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class KeepOnScreen : MonoBehaviour {

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
        Vector3 pos = Camera.main.WorldToViewportPoint(transform.position);
        if (pos.y > 1.0f)
        {
            Vector3 vel = GetComponent<Rigidbody>().velocity;
            vel.y = Camera.main.GetComponentInParent<CameraScroller>().GetSpeed();
            GetComponent<Rigidbody>().velocity = vel;
        }
        pos.x = Mathf.Clamp01(pos.x);
        pos.y = Mathf.Clamp01(pos.y);

        Vector3 worldOffset = Camera.main.ViewportToWorldPoint(pos) - Camera.main.transform.position;
        Vector3 actualOffset = transform.position - Camera.main.transform.position;
        transform.position = Camera.main.transform.position + (worldOffset * (actualOffset.z / worldOffset.z));
        
    }
}
