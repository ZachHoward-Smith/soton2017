﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class FallingObs : BaseObsticle
{
    Image WarningSprite;

    public bool DestroyOnContact;
    public float Lifetime;

    public AudioClip collisionNoise;

    void Update()
    {
        DisplayWarning();
        Lifetime -= Time.deltaTime;
        if(Lifetime <= 0)
        {
            Destroy(gameObject);
        }
    }

    protected override void OnCollisionEnter(Collision col)
    {
        // test if it's a player.
        col.collider.SendMessage("ApplyDamage", Damage, SendMessageOptions.DontRequireReceiver);
        if (collisionNoise != null)
        {
            GameManager.GM.GetComponent<AudioSource>().PlayOneShot(collisionNoise);
        }
        // Destroy this.
        if (DestroyOnContact)
        {
            Destroy(gameObject);
        }
    }

    void DisplayWarning()
    {
        // test if it's off screen.
        Vector3 viewportPos = ObsticleController.OC.MainCamera.WorldToViewportPoint(transform.position);
        if (viewportPos.x > 1 || viewportPos.y > 1 || viewportPos.x < 0 || viewportPos.y < 0)
        {
            // if it's above the parent obj
            if(transform.position.y > ObsticleController.OC.transform.position.y)
            {
                if (WarningSprite == null)
                {
                    WarningSprite = Instantiate((UseRedWarning) ? ObsticleController.OC.RedWarning : ObsticleController.OC.Warningprite, ObsticleController.OC.canvas.transform);
                    WarningSprite.rectTransform.rotation = Quaternion.identity;
                    WarningSprite.rectTransform.localScale = new Vector3(1, 1, 1);
                }

                // place the warning in the rocket column on the correct side at the right hight.
                WarningSprite.GetComponent<RectTransform>().anchoredPosition3D = Vector3.zero;
                //ObsticleController.OC.canvas
                WarningSprite.GetComponent<RectTransform>().anchoredPosition3D += new Vector3(((viewportPos.x * ObsticleController.OC.canvas.pixelRect.width) - (ObsticleController.OC.canvas.pixelRect.width / 2f)), (0.9f * ObsticleController.OC.canvas.pixelRect.height) - (ObsticleController.OC.canvas.pixelRect.height / 2f), 0);
            }
        }
        else
        {
            RemoveWarning();
        }

    }

    void RemoveWarning()
    {
        if (WarningSprite != null)
        {
            Destroy(WarningSprite.gameObject);
        }
    }

    void OnDestroy()
    {
        RemoveWarning();
    }

}
