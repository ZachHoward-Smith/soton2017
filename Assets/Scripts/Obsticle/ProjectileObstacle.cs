﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ProjectileObstacle : BaseObsticle
{
    [HideInInspector]
    public float Speed;

    public float SpeedModifier;
    public float DamageModifier;
    public float Lifetime;

    public AudioClip collisionNoise;

    Image WarningSprite;

	// Use this for initialization
	void Start ()
    {
        GetComponent<Rigidbody>().AddForce(transform.forward * Speed, ForceMode.VelocityChange);
        StartCoroutine(CanIDieYet());
	}

    void Update()
    {
        DisplayWarning();
    }

    IEnumerator CanIDieYet()
    {
        while (Lifetime > 0)
        {
            Lifetime -= Time.deltaTime;
            yield return null;
        }
        //RemoveWarning();
        Destroy(this.gameObject);
    }

    protected virtual void DisplayWarning()
    {
        // test if it's off screen.
        Vector3 viewportPos = ObsticleController.OC.MainCamera.WorldToViewportPoint(transform.position);
        if (viewportPos.x > 1 || viewportPos.y > 1 || viewportPos.x < 0 || viewportPos.y < 0)
        {
            // if it's moveing towards the center
            if (Vector3.Angle(transform.forward, ObsticleController.OC.MainCamera.ViewportToWorldPoint(new Vector3(0.5f, 0.5f, 30)) - transform.position) < 30)
            {
                int side = 1;
                //if it's on the left set a -1 variable;
                if (viewportPos.x < 0)
                {
                    side = -1;
                }

                if (WarningSprite == null)
                {
                    WarningSprite = Instantiate(ObsticleController.OC.Warningprite, ObsticleController.OC.canvas.transform);
                    WarningSprite.rectTransform.localScale = new Vector3(1, 1, 1);
                    WarningSprite.rectTransform.rotation = Quaternion.identity;
                }

                // place the warning in the rocket column on the correct side at the right hight.
                WarningSprite.GetComponent<RectTransform>().anchoredPosition3D = Vector3.zero;
                WarningSprite.GetComponent<RectTransform>().anchoredPosition3D += new Vector3(((0.95f * ObsticleController.OC.canvas.pixelRect.width) - (ObsticleController.OC.canvas.pixelRect.width / 2f)) * side, (viewportPos.y * ObsticleController.OC.canvas.pixelRect.height) - (ObsticleController.OC.canvas.pixelRect.height / 2f), 0);
            }
        }
        else
        {
            RemoveWarning();
        }

    }

    void RemoveWarning()
    {
        if (WarningSprite != null)
        {
            Destroy(WarningSprite.gameObject);
        }
    }

    void OnDestroy()
    {
        if (collisionNoise != null)
        {
            GameManager.GM.GetComponent<AudioSource>().PlayOneShot(collisionNoise);
        }
        RemoveWarning();
    }

}
