﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BaseObsticle : MonoBehaviour
{
    public ObsticleController.ObsticleType ObstacleType;
    public float Damage;
    public bool UseRedWarning;

    protected virtual void OnCollisionEnter(Collision col)
    {
        // test if it's a player.
        col.collider.SendMessage("ApplyDamage", Damage, SendMessageOptions.DontRequireReceiver);

        // Destroy this.
        Destroy(gameObject);
    }

    protected virtual void OnTriggerEnter(Collider other)
    {
        other.SendMessage("ApplyDamage", Damage, SendMessageOptions.DontRequireReceiver);
    }

}
