﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using Condition;
using SM;

public class WreckingBallObst : BaseObsticle
{
    public SpriteRenderer WarningSprite;

    public float WarningTime;
    public float WarningSizeMultiplyer;
    StateMachine WreckingBallMachine;
    Image Warning;
    Animator anim;

    void Start()
    {
        anim = GetComponent<Animator>();
        anim.enabled = false;
        SetupStateMachine();
    }

    void Update()
    {
        WreckingBallMachine.SMUpdate();
    }

    #region State Machine Setup

    void BeginWarning()
    {
        Vector3 WarningPos = transform.position + (Vector3.up * -29.3f);
        Warning = Instantiate(ObsticleController.OC.RedWarning, ObsticleController.OC.canvas.transform);
        WarningPos = ObsticleController.OC.MainCamera.WorldToViewportPoint(WarningPos);
        Warning.rectTransform.localScale = new Vector3(1, 1, 1);
        Warning.rectTransform.rotation = Quaternion.identity;
        WarningPos.z = 0;
        Warning.GetComponent<RectTransform>().anchoredPosition3D = Vector3.zero;
        Warning.GetComponent<RectTransform>().anchoredPosition3D += new Vector3((WarningPos.x * ObsticleController.OC.canvas.pixelRect.width) - (ObsticleController.OC.canvas.pixelRect.width / 2f), (WarningPos.y * ObsticleController.OC.canvas.pixelRect.height) - (ObsticleController.OC.canvas.pixelRect.height / 2f), 0);

    }

    void WarningUpdate()
    {
        WarningTime -= Time.deltaTime;
    }

    void EndWarning()
    {
        // remove Warning Sprite
        Destroy(Warning.gameObject);
    }

    void BeginWrecking()
    {
        anim.enabled = true;
    }

    void WreckingUpdate()
    {
        AnimatorStateInfo animInfo = anim.GetCurrentAnimatorStateInfo(0);
        if (animInfo.normalizedTime > 0.9f) Destroy(gameObject);
    }

    bool HasWarningTimeExpired()
    {
        return WarningTime <= 0;
    }

    #endregion

    void SetupStateMachine()
    {
        // create Conditions
        BoolCondition WarningTimeExpiredCond = new BoolCondition(HasWarningTimeExpired);

        // create transisions
        Transition WarningExpired = new Transition("Warning Expired", WarningTimeExpiredCond);

        // create States
        State WarningState = new State("Warning",
            new List<Transition>() { WarningExpired },
            new List<Action>() { BeginWarning },
            new List<Action>() { WarningUpdate },
            new List<Action>() { EndWarning });

        State WreckingFace = new State("Wrecking",
            null,
            new List<Action>() { BeginWrecking },
            new List<Action>() { WreckingUpdate },
            null);

        // add target States
        WarningExpired.SetTargetState(WreckingFace);

        // setup state machine
        WreckingBallMachine = new StateMachine(null, WarningState, WreckingFace);
        WreckingBallMachine.InitMachine();
    }

}
