﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using Condition;
using SM;

public class ObsticleController : MonoBehaviour
{
    public static ObsticleController OC;

    public Camera MainCamera;
    public List<BaseObsticle> ObsticlePrefabs = new List<BaseObsticle>();
    public Image Warningprite;
    public Image RedWarning;
    public Canvas canvas;
    public Collider[] SpawnZones;
    public enum ObsticleType { Projectile, Falling, Scenery }
    public float MinSpeeds, MaxSpeeds;
    public float timeTillGuarenteedObstacle;
    public float ObstacleSpawnCheackDelay;
    public CurveProperties curve;
    public bool gameisOver;

    bool gameStarted = false;
    StateMachine ControllerMachine;
    float timer = 0;
    float TotalTimer = 0;

    void Start()
    {
        OC = this;
        //StartCoroutine(CheckForSpawn());
        MainCamera = Camera.main;
        curve.maxInput = 30;
        SetupStateMachine();
    }

    void Update()
    {
        ControllerMachine.SMUpdate();
    }

    void WreckingUpdate()
    {
        timer += Time.deltaTime;
        TotalTimer += Time.deltaTime;
        curve.c = TotalTimer / timeTillGuarenteedObstacle;
    }

    void BeginWreckingState()
    {
        StartCoroutine(CheckForSpawn());
    }

    IEnumerator CheckForSpawn()
    {
        while (!gameisOver)
        {
            //print(timer + " " + curve.GetValue(timer));
            //if (Random.value < curve.GetValue(timer))
            if(Random.value < curve.GetValue(timer)) // 10% chance per delay.
            {
                // create distraction.
                // probobally falling or projectile.
                CreateObstacle((Random.value < 0.3f) ? ObsticleType.Scenery : (Random.value < 0.5f) ? ObsticleType.Falling : ObsticleType.Projectile);

                // reset timer values.
                timer = 0;
            }

            yield return new WaitForSeconds(ObstacleSpawnCheackDelay);
        }
    }

    void CreateObstacle(ObsticleType type)
    {
        
        List<BaseObsticle> availableObs = ObsticlePrefabs.FindAll(x => type == x.ObstacleType);
        Vector3 SpawnPoint = Vector3.zero;

        if (availableObs.Count > 0)
        {
            BaseObsticle nObstacle = Instantiate(availableObs[Random.Range(0, availableObs.Count)]);
            nObstacle.transform.SetParent(transform, true);
            switch (nObstacle.ObstacleType)
            {
                case ObsticleType.Projectile:
                    // place on side aim to near middle.
                    ProjectileObstacle nProj = (ProjectileObstacle)nObstacle;
                    SpawnPoint = MainCamera.ViewportToWorldPoint(new Vector3(Random.Range(1.2f, 1.7f) * ((Random.value > 0.5f) ? 1 : -1), Random.Range(0.2f, 0.8f), 30));

                    nObstacle.transform.position = SpawnPoint;
                    nObstacle.transform.position = Vector3.Scale(nObstacle.transform.position, new Vector3(1, 1, 0));
                    nProj.transform.LookAt(MainCamera.ViewportToWorldPoint(new Vector3(0.5f, 0.5f, 30)));

                    nProj.Speed = Random.Range(MinSpeeds, MaxSpeeds) * nProj.SpeedModifier;
                    break;

                case ObsticleType.Falling:
                    // place above screen, let fall.
                    SpawnPoint = MainCamera.ViewportToWorldPoint(new Vector3(Random.Range(0.3f, 0.6f), Random.Range(1.5f, 3f), 30));
                    nObstacle.transform.position = SpawnPoint;
                    nObstacle.transform.position = Vector3.Scale(nObstacle.transform.position, new Vector3(1, 1, 0));
                    if (nObstacle.UseRedWarning)
                    {
                        nObstacle.transform.position += Vector3.up * 5;
                    }
                    break;

                case ObsticleType.Scenery:

                    // place on screen.
                    SpawnPoint = MainCamera.ViewportToWorldPoint(new Vector3(Random.Range(0.3f, 0.6f), Random.Range(0.3f, 0.6f), 30));
                    SpawnPoint += Vector3.up * 29.3f;

                    nObstacle.transform.position = SpawnPoint;
                    nObstacle.transform.position = Vector3.Scale(nObstacle.transform.position, new Vector3(1, 1, 0));
                    break;
            }
            
        }
    }

    public void StartGame()
    {
        gameStarted = true;
    }

    void BeginGameTransFunc()
    {
        gameStarted = false;
    }

    void EndWreck()
    {
        //print("No more wrecking");
        gameisOver = false;
    }

    bool CanIStartWreckingFace()
    {
        return gameStarted;
    }

    bool GameisOver()
    {
        return gameisOver;
    }

    void SetupStateMachine()
    {
        // setup conditions
        BoolCondition canIStart = new BoolCondition(CanIStartWreckingFace);
        BoolCondition isgameOver = new BoolCondition(GameisOver);

        // setup transitions
        Transition BeginWrecking = new Transition("Begin Wrecking", canIStart, BeginGameTransFunc);
        Transition EndWrecking = new Transition("End Wrecking", isgameOver, EndWreck);

        // setup states
        State Waiting = new State("Waiting",
            new List<Transition>() { BeginWrecking },
            null,
            null,
            null);

        State Wrecking = new State("Wrecking",
            new List<Transition>() { EndWrecking },
            new List<Action>() { BeginWreckingState },
            new List<Action>() { WreckingUpdate },
            null);

        // add target states.
        BeginWrecking.SetTargetState(Wrecking);
        EndWrecking.SetTargetState(Waiting);

        // setup state machine.
        ControllerMachine = new StateMachine(null, Waiting, Wrecking);
        ControllerMachine.InitMachine();
    }

}

[System.Serializable]
public class CurveProperties
{
    public float m, k, p, c, d;
    public enum CurveTypes { Linear, Polynomial, Log, Trigonometric }
    public CurveTypes CurveType;

    public float minInput, maxInput;

    public CurveProperties(float M, float K, float P, float C, float D)
    {
        m = M;
        k = K;
        p = P;
        c = C;
        d = D;
    }

    public float GetValue(float input)
    {
        input = (input - minInput) / (maxInput - minInput);

        switch (CurveType)
        {
            case CurveTypes.Linear:
            case CurveTypes.Polynomial:
            default:
                return SamplePolyCurve(input);

            case CurveTypes.Log:
                return SampleLogCurve(input);

            case CurveTypes.Trigonometric:
                return SampleTrigCurve(input);
        }
    }

    float SamplePolyCurve(float x)
    {
        return (m * Mathf.Pow((x / d) + k, p)) + c;
    }

    float SampleLogCurve(float x)
    {
        return (m * Mathf.Log((x / d) + k, p)) + c;
    }

    float SampleTrigCurve(float x)
    {
        return (m * Mathf.Pow(Mathf.Sin((x / d) + k), 2)) + c;
    }

}
