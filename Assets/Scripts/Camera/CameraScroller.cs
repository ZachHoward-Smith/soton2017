﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraScroller : MonoBehaviour
{

    public float cameraStartSpeed = 1.0f;
    public float cameraSpeedUpRate = 0.1f;

    public Vector3 cameraMoveVector = Vector3.up;

    public HydrantManager hydrantManager;
    public SplashManagement waterInstance;

    public float cameraSpeed = 0.0f;

    private bool isPlaying = false;
    private bool shouldTransition = false;
    private bool inTransit = false;

    private Vector3 startPosition = new Vector3(0, 15, -21);
    private Vector3 endPosition = new Vector3(0, 26, -30);

    private const float TRANSITIONSPEED = 3.0F;
    //15, -21 - > 26 -30

    // Use this for initialization
    void Start ()
    {
        transform.position = startPosition;
        cameraSpeed = cameraStartSpeed;


	}
	
	// Update is called once per frame
	void FixedUpdate () {
        if (isPlaying)
        {
            //move the camera
            transform.position += cameraMoveVector * cameraSpeed * Time.fixedDeltaTime;
            //speed up the 
            cameraSpeed += cameraSpeedUpRate * Time.fixedDeltaTime;
            cameraSpeed = Mathf.Clamp(cameraSpeed, 0, 10);
        }
        else
        {
            if (shouldTransition)
            {
                if(!inTransit)
                {
                    inTransit = true;
                    hydrantManager.Initialize();
                    waterInstance.Initialize();
                }

                if((transform.position - endPosition).magnitude < 0.1f)
                {
                    transform.position = endPosition;
                    isPlaying = true;
                }
                else
                {
                    transform.position = Vector3.Lerp(transform.position, endPosition, Time.fixedDeltaTime * TRANSITIONSPEED);
                }
            }
        }
	}

    public void BeginGame()
    {
        shouldTransition = true;
    }

    public float GetSpeed()
    {
        return cameraSpeed;
    }
}
