﻿Shader "Custom/PropBlur" {
	Properties {
		_AlphaTex("Alpha", 2D) = "white" {}
		_Blend("Blend", Range(0,1)) = 0.0
	}
	SubShader {
		Tags{"QUEUE" = "Transparent" "IGNOREPROJECTOR" = "true" "RenderType" = "Transparent" "PreviewType" = "Plane" }
		LOD 200
		
		Cull Off
		ZWrite Off
		Blend SrcAlpha OneMinusSrcAlpha
		ColorMask RGB
		
		CGPROGRAM
		// Physically based Standard lighting model, and enable shadows on all light types
		#pragma surface surf SimpleSpecular alpha 
		// Use shader model 3.0 target, to get nicer looking lighting
		//#pragma target 3.0

		half4 LightingSimpleSpecular(SurfaceOutput s, half3 lightDir, half3 viewDir, half atten) 
		{
			half3 h = normalize(lightDir + viewDir);
			half diff = max(0, dot(s.Normal, lightDir));
			float nh = max(0, dot(s.Normal, h));
			float spec = pow(nh, 48.0);
			half4 c;
			c.rgb = s.Albedo.rgb; //(s.Albedo * _LightColor0.rgb * diff + _LightColor0.rgb * spec * s.Alpha /** _Shininess*/ * _SpecColor) * (atten * 2);
			c.a = s.Alpha;
			return c;
		}


		sampler2D _AlphaTex;

		struct Input {
			float2 uv_AlphaTex;
		};

		half _Blend;
		

		void surf (Input IN, inout SurfaceOutput o) {
			// Albedo comes from a texture tinted by color
			fixed4 c = tex2D (_AlphaTex, IN.uv_AlphaTex).rgba;
			o.Albedo = float3(0.0,0.0,0.0);
			//o.Metallic = 0;
			//o.Smoothness = 1;
			o.Alpha = pow(lerp(0.0, c.r * _Blend, _Blend),2.0) - 0.1;
		}

		
		ENDCG
	}
	FallBack "Diffuse"
}
