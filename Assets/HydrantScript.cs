﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HydrantScript : MonoBehaviour
{
    public GameObject hydrant1;
    public GameObject hydrant2;
    // Use this for initialization
    void Start ()
    {
		
	}
	
	// Update is called once per frame
	void Update ()
    {
		
	}
    
    public void Initialize()
    {
        hydrant1.SetActive(true);
        hydrant2.SetActive(true);
    }

    public void Terminate()
    {
        hydrant1.SetActive(false);
        hydrant2.SetActive(false);
    }
}
